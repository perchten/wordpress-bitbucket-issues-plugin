<?php

require('bitbucket-api.php');

class BitbucketBroker {
	
	var $accountname, $repo_slug;
	var $type, $access;
	
	var $key, $secret, $links;
	var $cache, $cache_path;
	
	var $filter, $filter_plain;
	var $items, $resolvedItems;
	var $url;
 
	function __construct($accountname, $repo_slug, $type, $access, $filter, $cache = 0) {
		$this->accountname = str_replace(' ', '-', $accountname);
		$this->repo_slug = strtolower(str_replace(' ', '-', $repo_slug));
		
		$this->type = $type;
		$this->access = $access;

    $options = get_option('bbi_options');
		$this->key = $this->access == 'private' ? $options['key'] : false;
		$this->secret = $this->access == 'private' ? $options['secret'] : false;
		
		$this->cache = strtolower($cache) == 'on' ? PHP_INT_MAX : $cache;
		$this->cache_path = ABSPATH . trim($options['cache_path'], '/') . '/';

		if (!file_exists($this->cache_path))
			mkdir($this->cache_path, 0777, true);

		$this->links = $options['links'];
		$this->filter = array_filter($filter);

    // Build filter string
    $array = array();
    foreach ($filter as $key => $value) {
        if ($value) {
            foreach (explode(',', $value) as $value) {
                $array[] = $key . '=' . $value;
            }
        }
    }
    $this->filter_plain = count($array) ? '?' . implode("&", $array) : '';

		if ($type == 'changelog' || $type == 'roadmap')
			$this->url = ($this->links == 1 || ($this->links != 2 && $this->access == 'public')) ? BitbucketApi::$urlBase . $this->accountname . '/' . $this->repo_slug . '/issues' . $this->filter_plain : '';

	}

	function cache_name() {
		return $this->access . '_' . $this->type . '_' . $this->accountname . '_' . $this->repo_slug . ($this->filter_plain ? '_' . $this->filter_plain : '') . '.cache';
	}

	function issues() {
		// Return issues staight away if available
		if ($this->items)
			return $this->items;
		$cache_file = $this->cache_path . $this->cache_name();
		if (is_file($cache_file) && ((time() - filemtime($cache_file)) < $this->cache)) {
			$data = json_decode(file_get_contents($cache_file));
		}
		else {
			// Fetch data
			$fields = array(
				'accountname' => $this->accountname,
				'repo_slug' => $this->repo_slug
			);
			$brocker = new BitbucketApi($fields, "issues", $this->filter, $this->key, $this->secret);
			$raw_data = $brocker->request();
			$data = json_decode($raw_data);
			if (!is_numeric($data) && $data)
				file_put_contents($cache_file, $raw_data);
		}
		if (is_numeric($data)) {
			$this->items = array();
			$this->resolvedItems = 0;
			return $data;
		}
		elseif ($data) {			
			$this->items = array();
			$this->resolvedItems = 0;
			foreach ($data->issues as $issue) {
				$issue =  new BitbucketIssue($this, $issue);
				$this->resolvedItems += $issue->status == "resolved" ? 1 : 0;
				$this->items[] = $issue;
			}
			return $this->items;
		}
		return 401;
	}

	function changesets($type = "changesets") {
		// Return issues staight away if available
		if ($this->items)
			return $this->items;
		$cache_file = $this->cache_path . $this->cache_name();
		if (is_file($cache_file) && ((time() - filemtime($cache_file)) < $this->cache)) {
			$data = json_decode(file_get_contents($cache_file));
		}
		else {
			// Fetch data
			$fields = array(
				'accountname' => $this->accountname,
				'repo_slug' => $this->repo_slug
			);
			$brocker = new BitbucketApi($fields, "changesets", $this->filter, $this->key, $this->secret);
			$raw_data = $brocker->request();
			$data = json_decode($raw_data);
			if (!is_numeric($data) && $data)
				file_put_contents($cache_file, $raw_data);
		}
		if (is_numeric($data)) {
			$this->items = array();
			return $data;
		}
		elseif ($data) {
			foreach (array_reverse($data->changesets) as $changeset) {
				$changeset =  new BitbucketChangeset($this, $changeset);
				$this->items[] = $changeset;
			}
			return $this->items;
		}
		else {
			$this->items = array();
		}
		return 401;
	}
}

class BitbucketIssue {

		var $id, $title, $description, $created, $updated, $kind, $priority, $status, $assignee, $version, $milestone, $component;
		var $url, $search, $replace;

		function __construct($broker, $issue) {
			$this->id = $issue->local_id;
			$this->title = $issue->title;
			$this->description = $issue->content;
			$this->created = date_format(date_create($issue->utc_created_on) ,'d-m-Y');
			$this->updated = date_format(date_create($issue->utc_last_updated) ,'d-m-Y');
			$this->kind = $issue->metadata->kind;
			$this->priority = $issue->priority;
			$this->status = $issue->status;
			$this->assignee = $issue->responsible ? $issue->responsible->accountname : "unassigned";
			$this->version = $issue->metadata->version;
			$this->milestone = $issue->metadata->milestone;
			$this->component = $issue->metadata->component;

			$this->url = ($broker->links == 1 || ($broker->links != 2 && $broker->access == 'public')) ? BitbucketApi::$urlBase . $broker->accountname . '/' . $broker->repo_slug . '/issue/' . $this->id : '';
			$this->search = array("{title}", "{description}", "{created}", "{updated}", "{kind}", "{priority}", "{status}", "{assignee}", "{version}", "{milestone}", "{component}");
			$this->replace = array($this->title, $this->description, $this->created, $this->updated, ucwords($this->kind), $this->priority, $this->status, $this->assignee, $this->version, $this->milestone, $this->component);
		}
}

class BitbucketChangeset {

	var $node, $raw_node, $shortnode, $author, $timestamp, $branch, $message;
	var $url, $search, $replace;

	function __construct($broker, $changeset) {
		$this->node = $changeset->node;
		$this->raw_node = $changeset->raw_node;
		$this->commit = substr($changeset->node, 0, 7);
		$this->author = $changeset->author;
		$this->date = date_format(date_create($changeset->utctimestamp) ,'d-m-Y');
		$this->branch = trim($changeset->branch);
		$this->message = $changeset->message;

		$this->url = ($broker->links == 1 || ($broker->links != 2 && $broker->access == 'public')) ? BitbucketApi::$urlBase . $broker->accountname . '/' . $broker->repo_slug . '/commits/' . $this->raw_node : '';
		$this->search = array("{node}", "{raw_node}", "{commit}", "{author}", "{date}", "{branch}", "{message}", "{timestamp}");
		$this->replace = array($this->node, $this->raw_node, $this->commit, $this->author, $this->date, $this->branch, $this->message, $this->date);
	}
}

?>
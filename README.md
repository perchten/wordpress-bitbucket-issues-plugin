# Installation

Check out the [official installation guide](http://codex.wordpress.org/Managing_Plugins). It's totally more awesome than anything I'd be bothered to write here. Plus, Wordpress codex is your friend. (Mostly).

# About the Bitbucket Plugin

You must provide options in the Shortcode, to include information of Bitbucket you have use:
    
    [bitbucket issues private accountname=### repo_slug=###]

The first two parameters are mandatory and determines the type as well as the access level. Replace ### with your Bitbucket username (if you haven't set a default in the setting) and ### with the name of you repository.

## Available Types

### Issue List
    [bitbucket issues public accountname=### repo_slug=### status=!resolved]
### Issue List Plain
    [bitbucket issues_plain public accountname=### repo_slug=### status=!resolved]
### Changesets List
    [bitbucket changesets private accountname=### repo_slug=### status=!resolved]
### Changesets List Plain
    [bitbucket changesets_plain public accountname=### repo_slug=### status=!resolved]
### Changelog
    [bitbucket changelog public accountname=### repo_slug=### status=resolved milestone=1.2]
### Roadmap
    [bitbucket roadmap public accountname=### repo_slug=### milestone=1.3]

## Available Settings Wordpress Admin Panel

* **Accountname:** Default accountname
* **OAuth Key:** OAuth consumers key
* **OAuth Secret:** OAuth consumers secret
* **Cache:** Default cache duration
* **Cache Path:** Path for the cache files
* **Link Items to Bitbucket for...:** Show links for either public, private, all or no repositories
* **ID format:** Format of the ID; e.g. `#0000` leaded to number with at least 4 digits and a leading '#' character
* **Issue Item Format:** Format of an item inside an issue_plain
* **Changeset Item Format:** Format of an item inside a changeset_plain
* **Changelog Item Format:** Format of an item inside a changlog
* **Roadmap Item Format:** Format of an item inside a roadmap

## Available Parameters Shortcode

### accountname
    The team or individual account owning the repository.
### repo_slug
    The repository identifier.
### component
    A user defined enumerator value.
### content
    A string containing the issue description.
### kind
    This is equivalent to the Type value in the GUI. It can take any of the following values: bug, enhancement, proposal, task
### limit
    An integer specifying the number of issues to return. You can specify a value between 0 and 50. The default is 15.
### milestone
    A user defined enumerator value.
### priority:
    A enumerator representing the issue priority. Valid priority values are:, trivial, minor, , major, , critical, , blocker, 
### reported_by
    Contains the profile for the user that reported the issue.
### responsible
    Contains the profile for the user that reported the issue.
### sort: 
    Sorts the output by any of the metadata fields. Valid search values are: comment_count, component, content, created_on, follower_count, is_spam, local_id, milestone, priority, status, title, utc_created_on, utc_last_updated, version, -comment_count, -component, -content, -created_on, -follower_count, -is_spam, -local_id, -milestone, -priority, -status, -title, -utc_created_on, -utc_last_updated, -version
### start
    Offset to start at. The default is 0 (zero).
### status:
    A enumerator representing the issue status. Valid status values are: new, open, resolved, on hold, invalid, duplicate, wontfix
### title
    A string containing the issue title.
### version
	  A user defined enumerator value.

# Additional Informations Issues

Authorization is not required for public repositories with a public issue tracker. Private repositories or private issue trackers require the caller to authenticate with an account that has appropriate authorisation. By default, Bitbucket returns 15 issues. If necessary you can specify the sort parameter to order the output. 

You can supply any of the following parameters to the command:

Parameter | Required? | Description
-- | -- | --
accountname | Yes | The team or individual account owning the repository.
repo_slug | Yes |	The repository identifier.
component | No | Contains an is or ! (is not) filter   to restrict the list of issues.
content | No | Contains a filter operation to restrict the list of issues.
kind | No |  Contains an is or ! (is not) filter to restrict the list of issues.
limit | No | An integer specifying the number of issues to return. You can specify a value between 0 and 50. The default is 15.
milestone | No | Contains an is or ! (is not) filter to restrict the list of issues.
priority | No | Contains an is or ! (is not) filter to restrict the list of issues.
reported_by | No |Contains a filter operation to restrict the list of issues.
responsible | No |  Contains an is or ! (is not) filter to restrict the list of issues.
search | No | A string to search for.
sort | No | Sorts the output by any of the metadata fields. Enter a name value pair similar to the following: `sort=milestone`
start | No | Offset to start at. The default is 0 (zero).
status | No |  Contains an is or ! (is not) filter  to restrict the list of issues.
title | No | Contains a filter operation to restrict the list of issues.
version | No | Contains an is or ! (is not) filter to restrict the list of issues.

## Filter Options

### The Bitbucket service supports the following filter operators
Operator| Definition
-- | --
~ | contains
!~ | doesn't contain
^ | begins with
$ | ends with
! | is not
Not applicable | is

The filter operators do not work with all of the possible issue fields. The following table lists the fields you can filter on and which operators work with them.

Issue Field | Supported Filter Operators
-- | --
component | is and ! (is not)
content | All
kind | is and ! (is not)
milestone | is and ! (is not)
reported_by | All
responsible | is and ! (is not)
status |is and ! (is not)
title | All
version | is and ! (is not)

### Example

When constructing your command, combine one of the issue fields as a parameter and the operator in the parameter value. The following table illustrates some examples of how to construct a parametrized filter:

Parameter | Value | Notes
-- | --
status | open | status is open
kind | !bug | kind is not bug
title | ~work | title contains work


**What the filter will look like:**

`[bitbucket issues accountname=### repo_slug=### status=open kind=!bugs title=~work]`

# Additional Informations Changesets

Gets a list of change sets associated with a repository. By default, this call returns the 15 most recent changesets. Private repositories require the caller to authenticate. This shortcode takes the following parameters:

Parameter | Required? | Description
-- | -- | --
accountname | Yes | The team or individual account owning the repo.
repo_slug | Yes | The repo identifier.
limit | No | An integer representing how many changesets to return. You can specify a limit between 0 and 50. If you specify 0 (zero), the system returns the count but returns empty values for the remaining fields.
start | No | A hash value representing the earliest node to start with. The system starts with the specified node and includes the older requests that preceded it. The Bitbucket GUI lists the nodes on the Commit tab. The default start value is the tip.

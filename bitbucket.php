<?php
/*
Plugin Name: BitBucket
Description: Aggregate content from bitbucket repositories
Plugin URI: https://bitbucket.org/perchten/wordpress-bitbucket-plugin
Version: 0.3
License: GPL
Author: Perch Ten Design modified by Chris---
Author URI: perchten.co.uk
*/

require('bitbucket-broker.php');
require('bitbucket-theme.php');

add_shortcode('bitbucket', 'bitbucket_shortcode');
function bitbucket_shortcode($atts) {
	if (!in_array($atts[0], array('changesets', 'changesets_plain', 'issues', 'issues_plain', 'roadmap', 'changelog')) || !in_array($atts[1], array('private', 'public')))
		return BitbucketTheme::invalid();
	
	$id = "bitbucket_" . uniqid();
	$argument =  $id . ",'" . serialize($atts) . "','" . wp_create_nonce("bitbucket_nonce") . "'";
	return '<div id="' . $id . '" class="alert alert-info"><img src="' . plugins_url('img/loading.gif', __FILE__) . '""> Fetching data from Bitbucket.org</div><script type="text/javascript">bitbucket_load_repository(' . $argument . ');</script>';
}

add_action( 'wp_ajax_nopriv_bitbucket_ajaxhandler', 'bitbucket_ajaxhandler' );
add_action( 'wp_ajax_bitbucket_ajaxhandler', 'bitbucket_ajaxhandler' );
function bitbucket_ajaxhandler() {
	if (!wp_verify_nonce( $_POST['nonce'], "bitbucket_nonce"))
		exit("Wrong nonce");
	$atts = unserialize(stripslashes($_POST['atts']));

	$options = get_option('bbi_options');
	extract(shortcode_atts(array(
		'accountname' => $options['accountname'],
		'repo_slug' => '',
		'cache' => $options['cache'],
		'component' => '',
		'content' => '',
		'kind' => '',
		'limit' => '',
		'milestone' => '',
		'page' => '',
		'priority' => '',
		'reported_by' => '',
		'responsible' => '',
		'search' => '',
		'sort' => '-utc_last_updated',
		'start' => '',
		'status' => '',
		'title' => '',
		'version' => '',
	), $atts));

	// Just resolved issues for a changelog
	if ($atts[0] == 'changelog') {
		$status = 'resolved';
	}

	$broker = new BitbucketBroker($accountname, $repo_slug, $atts[0], $atts[1], array( 
		'component' => $component, 
		'content' => $content,
		'kind' => $kind,        
		'limit' => $limit,
		'milestone' => $milestone,
		'page' => $page,
		'priority' => $priority,
		'reported_by' => $reported_by,
		'responsible' => $responsible,
		'search' => $search,
		'sort' => $sort,
		'start' => $start,
		'status' => $status,
		'title' => $title,
		'version' => $version,
	), $cache);

	ob_start();
	if ($atts[0] == 'issues')
		BitbucketTheme::issues($broker->issues());
	elseif ($atts[0] == 'issues_plain')
		BitbucketTheme::issues_plain($broker->issues());
	if ($atts[0] == 'changesets')
		BitbucketTheme::changesets($broker->changesets());
	elseif ($atts[0] == 'changesets_plain')
		BitbucketTheme::changesets_plain($broker->changesets());
	elseif ($atts[0] == 'changelog')
		BitbucketTheme::changelog($repo_slug, $broker);
	elseif ($atts[0] == 'roadmap')
		BitbucketTheme::roadmap($repo_slug, $broker);

	$content = ob_get_contents();
	ob_get_clean();
	
	die($content);
}

add_action('admin_menu' , 'bitbucket_menu');
function bitbucket_menu() {
	add_options_page('BitBucket-Options', 'BitBucket', 'manage_options', 'bitbucket', 'bitbucket_page');
}

function bitbucket_page() {

	if (!current_user_can('manage_options'))
	  wp_die( __('You do not have sufficient permissions to access this page.'));

	$opt_name = 'bbi_options';
	$opt_field_name = 'bitbucketissues_options';
	$hidden_field_name = 'bitbucketissues_submit_hidden';

	if (array_key_exists($hidden_field_name, $_POST)) {
		update_option($opt_name, $_POST[$opt_field_name]);
		?><div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div><?php
	}

	$options = get_option($opt_name);

	?>

	<div class="wrap">
		<?php screen_icon(); ?>
		<h2>BitBucket Settings</h2>
		<h4>Authentication Details</h4>
		<form method="post">
			<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">            
			<table class="form-table">
				<tr align="top">
					<th scope="row">Accountname</th>
					<td><input type="text" name="bitbucketissues_options[accountname]" value="<?php echo $options['accountname'] ?>" size="30" /></td>
				</tr>
				<tr align="top">
					<th scope="row">OAuth Key</th>
					<td><input type="text" name="bitbucketissues_options[key]" value="<?php echo $options['key'] ?>" size="30" /></td>
				</tr>
				<tr align="top">
					<th scope="row">OAuth Secret</th>
					<td><input type="text" name="bitbucketissues_options[secret]" value="<?php echo $options['secret'] ?>" size="30" /></td>
				</tr>
				<tr align="top">
					<th scope="row">Cache Duration</th>
					<td><input type="text" name="bitbucketissues_options[cache]" value="<?php echo $options['cache'] ? $options['cache'] : BitbucketTheme::$cache; ?>" size="5" /></td>
				</tr>
				<tr align="top">
					<th scope="row">Cache Path</th>
					<td><input type="text" name="bitbucketissues_options[cache_path]" value="<?php echo $options['cache_path'] ? $options['cache_path'] : BitbucketTheme::$cache_path; ?>" size="30" /></td>
				</tr>
				<tr align="top">
					<th scope="row">Link Items to Bitbucket for...</th>
					<td>
						<fieldset>
							<input type="radio" name="bitbucketissues_options[links]" value="0" <?php echo checked(0 , $options['links'] ? $options['links'] : 0) ?> />
							<label>Public Repositories</label><br/>
							<input type="radio" name="bitbucketissues_options[links]" value="1" <?php echo checked(1 , $options['links']) ?> />
							<label>Public and Private Repositories</label><br/>
							<input type="radio" name="bitbucketissues_options[links]" value="2" <?php echo checked(2 , $options['links']) ?> />
							<label>None</label>
						</fieldset>
					</td>
				</tr>
				<tr align="top">
					<th scope="row">ID format</th>
					<td><input type="text" name="bitbucketissues_options[id_format]" value="<?php echo $options['id_format'] ? $options['id_format'] : BitbucketTheme::$id_format; ?>" size="5" /></td>
				</tr>
			</table>
			<h4>Changelog and Roadmap</h4>
			<table class="form-table">
			<tr align="top">
				<th scope="row">Issue Item Format</th>
				<td><input type="text" name="bitbucketissues_options[issue_format]" value="<?php echo $options['issue_format'] ? $options['issue_format'] : BitbucketTheme::$issue_format; ?>" size="50" /></td>
			</tr>
			<tr align="top">
				<th scope="row">Changeset Item Format</th>
				<td><input type="text" name="bitbucketissues_options[changeset_format]" value="<?php echo $options['changeset_format'] ? $options['changeset_format'] : BitbucketTheme::$changeset_format; ?>" size="50" /></td>
			<tr align="top">
				<th scope="row">Changelog Item Format</th>
				<td><input type="text" name="bitbucketissues_options[changelog_format]" value="<?php echo $options['changelog_format'] ? $options['changelog_format'] : BitbucketTheme::$changelog_format; ?>" size="50" /></td>
			</tr>
			<tr align="top">
				<th scope="row">Roadmap Item Format</th>
				<td><input type="text" name="bitbucketissues_options[roadmap_format]" value="<?php echo $options['roadmap_format'] ? $options['roadmap_format'] : BitbucketTheme::$roadmap_format; ?>" size="50" /></td>
			</tr>
			</table>
			<?php submit_button(); ?>
			<h4>Issue Table</h4>
			<?php echo BitbucketTheme::admin_page_table($options['issue_table'] ? $options['issue_table'] : BitbucketTheme::$issue_table, 'issue_table'); ?> 
			<h4>Changeset Table</h4>
			<?php echo BitbucketTheme::admin_page_table($options['changeset_table'] ? $options['changeset_table'] : BitbucketTheme::$changeset_table, 'changeset_table'); ?> 
			<?php submit_button(); ?>
		</form>
	</div>
	<?php
}

function bitbucket_enqueue_scripts() {
	wp_enqueue_style('bitbucket', plugins_url('bitbucket.css', __FILE__));
	wp_enqueue_script('bitbucket', plugins_url('lib/ajax.js', __FILE__), array('jquery'), '1.9.1');
	wp_localize_script('bitbucket', 'bitbucketajax', array('ajaxurl' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'bitbucket_enqueue_scripts');

?>